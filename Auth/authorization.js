const expressJwt = require('express-jwt');
const config = require('../config');

const authorization = expressJwt({
    secret: config.secret,
    getToken: (req) => {
        if (req.query.token && req.query.token.split(' ')[0] === 'Bearer')
            return req.query.token.split(' ')[1];
        else
            return null;
    }
});
// .unless({ path: [/\/course\/*/] });

module.exports = authorization;