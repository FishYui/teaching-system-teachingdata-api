const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const authorization = require('./Auth/authorization');
const app = express();
const config = require('./config');

app.use(cookieParser());
app.use(cors({
    origin: [config.portalUrl],
    credentials: true
}));

//授權驗證
app.use(authorization);
app.use((err, req, res, next) => {
    if (err.name === 'UnauthorizedError') {
        res.status(err.status).json({
            'success': false,
            'message': `Token Auth Error, The Err: ${err.message$}`
        });
    }
});

app.use('/resource', require('./routers/resource'));

const PORT = process.env.PORT || 6002;
app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
});