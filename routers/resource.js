const express = require('express');
const Url = require('url');
const fs = require('fs');

const router = express.Router();

router.get('/download', (req, res) => {

    const scope = req.user.scope;
    const url = Url.parse(scope.url);
    const path = decodeURI(`./file${url.pathname}`);

    fs.exists((path), (status) => {
        if (status) {
            res.download(path);
        } else {
            res.json({ 'success': 'false', 'message': 'No Found file.' });
        }
    });
});

module.exports = router;