const config = {
    'secret': 'e802107e3c8a2fe5658b88cef4ec429f',
    'useFor': 'TCU-H707',
    'expiresIn': 60 * 15,

    'fhirServer': 'http://203.64.84.213:8080/hapi-fhir-jpaserver/fhir',

    //local
    'portalUrl': 'http://localhost:5500',

    //server
    // 'portalUrl': 'http://203.64.84.213:5500',
    
}

module.exports = config;